local theme = require("config").theme.theme

vim.cmd([[
  try
    colorscheme ]] ..theme.. [[\
  catch /^Vim\%((\a\+)\)\=:E185/
    colorscheme default
    set background=dark
  endtry
]])
