local M = {
  bg = "#161320",
  fg = "#D9E0EE",
  yellow = "#FAE3B0",
  cyan = "#89DCEB",
  darkblue = "96CDFB",
  green = "#ABE9B3",
  orange = "#F8BD96",
  violet = "#DDB6F2",
  magenta = "#E8A2AF",
  blue = "#96CDFB",
  red = "#F28FAD",
}

return M
