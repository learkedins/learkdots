local status_ok, packer = pcall(require, "packer")
if not status_ok then
	return
end

packer.init {
  display = {
    open_fn = function()
      return require('packer.util').float { border = "rounded" }
    end,
  }
}

return packer.startup(function(use)

  -- Essentials
  use "wbthomason/packer.nvim"
  use {
    "nvim-lua/popup.nvim",
    "nvim-lua/plenary.nvim",
    "numToStr/Comment.nvim",
    "kyazdani42/nvim-web-devicons",
    "kyazdani42/nvim-tree.lua",
  }

  -- Colorscheme
  use { "catppuccin/nvim", as = "catppuccin" }

  -- Lualine
  use { "nvim-lualine/lualine.nvim" }

  use {
    'romgrk/barbar.nvim',
    requires = {'kyazdani42/nvim-web-devicons'}
  }
end)
