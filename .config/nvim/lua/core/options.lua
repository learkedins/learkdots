local options = {
	number=true,
  clipboard=unnamedplus,
	syntax=enable,
	tabstop=2,
	softtabstop=2,
	shiftwidth=2,
	nocompatible=enable,
	cursorline=true,
	cursorcolumn=true,
	signcolumn="yes",
	showtabline=2,
	expandtab=true,
	backup=false,
	incsearch=true,
	ignorecase=true,
	smartcase=true,
	showmatch=true,
	hlsearch=true,
	history=1000,
  swapfile=false,
  undofile=true,
  laststatus=3
}

for k,v in pairs(options) do
	vim.opt[k] = v
end

local custom_options = require("config").options.config

for k,v in pairs(custom_options) do
  vim.opt[k] = v
end

local bg = require("config").theme.bg
local cmd = vim.cmd
cmd[[filetype on]]
cmd[[filetype plugin on]]
cmd[[filetype indent on]]
cmd(string.format("set background=%s", bg))
