local M = {}

M.theme = {
  theme = "catppuccin",
  bg = "dark"
}

M.options = {
  config = {
    termguicolors=true,
    wrap=false,
    mouse="a",
  }
}

return M
